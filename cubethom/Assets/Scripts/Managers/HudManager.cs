﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudManager : MonoBehaviour
{
	public static HudManager instance;

	[SerializeField] private GameObject startPanel;
	[SerializeField] private Panel endLevelPanel;
	[SerializeField] private GameObject gameHudPanel;

	

	private void Awake()
	{
		instance = this;
	}

	public void PanelUpdate(GameStatus _nextStatus)
	{
		switch(_nextStatus)
		{
			case GameStatus.start:
				startPanel.SetActive(true);
				gameHudPanel.SetActive(false);
				endLevelPanel.gameObject.SetActive(false);
				break;

			case GameStatus.runing:
				startPanel.SetActive(false);
				gameHudPanel.SetActive(true);
				endLevelPanel.gameObject.SetActive(false);
				break;

			case GameStatus.gameOver:
				startPanel.SetActive(false);
				gameHudPanel.SetActive(false);
				endLevelPanel.gameObject.SetActive(true);
				endLevelPanel.PanelUpdate(_nextStatus);
				break;

			case GameStatus.win:
				startPanel.SetActive(true);
				gameHudPanel.SetActive(false);
				endLevelPanel.gameObject.SetActive(true);
				endLevelPanel.PanelUpdate(_nextStatus);
				break;
		}
	}
}
