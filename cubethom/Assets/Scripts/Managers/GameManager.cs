﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public enum GameStatus
{
	start,
	runing,
	gameOver,
	win,
}

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public GameStatus CurrentGamestatus;

	public bool instantiatedEndLevel;

	private float _difficultModifier = 1;
	public float DifficultModifier { get { return _difficultModifier; } }
	[SerializeField] float difficultIncrement = 0.2f;


	public mapTile MapTilePrefab;
	[SerializeField] float TileSize;
	[SerializeField] int PlaneLength;
	private GameObject EnviromentContainer;
	private mapTile lastTile;

    [SerializeField] private float mapDefaultSpeed = 10;
	private float _mapSpeed;
	public float MapSpeed { get { return _mapSpeed; } set { if (value > 0 || _mapSpeed > minMapSpeed) { _mapSpeed = value; } } }

	private float minMapSpeed = 4;

	[SerializeField] int _mapTailToFinishLevel = 50;
	public int MapTailToFinishLevel
	{
		get
		{
			return _mapTailToFinishLevel;
		}
		set
		{
			if(value > _mapTailToFinishLevel)
			{
				_mapTailToFinishLevel = value;
			}
		}
	}

	private int _currentMapCount = 0;
	public int CurrentMapCount { get { return _currentMapCount; } }

	[SerializeField] PlayerController PlayerPrefab;
	PlayerController _currentPlayer;
	public PlayerController CurrentPlayer { get { return _currentPlayer; } }
	private float score = 0;

    [SerializeField] int topListNumber = 3;
    List<int> bestScores = new List<int>();
    

	private void Awake()
	{
		instance = this;
		_difficultModifier = 1;
	}

	private void Start()
	{
		ChangeStatus(GameStatus.start);
	}

	private void Update()
	{
		if(CurrentGamestatus == GameStatus.runing)
		{
			score += Time.deltaTime;
		}
	}

	public void Restart()
	{
		_currentMapCount = 0;
        _mapSpeed = mapDefaultSpeed;

		instantiatedEndLevel = false;

		if(EnviromentContainer)
		{
			Destroy(EnviromentContainer);
		}

		EnviromentContainer = new GameObject();

		_currentPlayer = Instantiate(PlayerPrefab, EnviromentContainer.transform);
		_currentPlayer.transform.position = Vector3.zero;

		for(int i = 0; i < PlaneLength; i++)
		{
			lastTile = Instantiate(MapTilePrefab, EnviromentContainer.transform);
			lastTile.transform.position += Vector3.forward * TileSize * i;
			lastTile.Setup();
			_currentMapCount++;
		}

		ChangeStatus(GameStatus.runing);
	}

	public void newTile()
	{
		mapTile currentTile = Instantiate(MapTilePrefab, EnviromentContainer.transform);
		currentTile.transform.position = lastTile.transform.position + Vector3.forward * TileSize;
		currentTile.Setup();
		lastTile = currentTile;
		_currentMapCount++;
	}

	private void ChangeStatus(GameStatus _nextStatus)
	{
		CurrentGamestatus = _nextStatus;
		HudManager.instance.PanelUpdate(CurrentGamestatus);
	}

	public void GameOver()
	{
		CurrentGamestatus = GameStatus.gameOver;
        bestScores.Add(Mathf.RoundToInt(score));
        bestScores = (from score in bestScores orderby score descending select score).ToList();
        if(bestScores.Count > topListNumber)
        {
            bestScores.Remove(bestScores[topListNumber]);
            Debug.Log(bestScores.Count());
        }
	}

	public void StopGame()
	{
		HudManager.instance.PanelUpdate(CurrentGamestatus);
	}

	public void OnClickResetGame()
	{
		_difficultModifier = 1;
        score = 0;
        Restart();
	}

	public void Win()
	{
		CurrentGamestatus = GameStatus.win;
	}

	public void NextLevel()
	{
		Debug.Log("Loading new level");
		_difficultModifier += difficultIncrement;
		Restart();
	}

	public int GetScore()
	{
		return Mathf.RoundToInt(score);
	}

    public List<int> GetTopScores()
    {
        return bestScores;
    }

	public void OnClickQuit()
	{
		Application.Quit();
	}
}
