﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreText : MonoBehaviour
{
	 private TextMeshProUGUI scoreTxt;

	private void Awake()
	{
		scoreTxt = GetComponent<TextMeshProUGUI>();
	}

	// Update is called once per frame
	void Update()
    {
		scoreTxt.text = GameManager.instance.GetScore().ToString();
    }
}
