﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class Panel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI bestScorePrefab;
    [SerializeField] private Transform topScoreContainer;

	[SerializeField] private TextMeshProUGUI panelTxt;
	[SerializeField] private TextMeshProUGUI endBtnText;

    private List<TextMeshProUGUI> currentTextList = new List<TextMeshProUGUI>();

	private GameStatus currentGameStatus;

    private void OnEnable()
    {
        SetScoreText();
    }

    public void PanelUpdate(GameStatus _nextStatus)
	{
		if(_nextStatus.Equals(GameStatus.gameOver))
		{
			panelTxt.text = "Game Over";
			endBtnText.text = "retry";
			currentGameStatus = _nextStatus;
		}
		else if(_nextStatus.Equals(GameStatus.win))
		{
			panelTxt.text = "Level complete";
			endBtnText.text = "next level";
			currentGameStatus = _nextStatus;
		}
	}

    private void SetScoreText()
    {
        foreach(var text in currentTextList)
        {
            Destroy(text.gameObject);
        }
        currentTextList.Clear();

        List<int> scores = GameManager.instance.GetTopScores();

        int index = 1;
        foreach(var score in scores)
        {
            var currentText = Instantiate(bestScorePrefab, topScoreContainer);
            currentText.text = "position " + index + ": " + score.ToString();
            currentTextList.Add(currentText);
            index++;
        }
    }

	public void OnClickEndBtn()
	{
		if(currentGameStatus.Equals(GameStatus.gameOver))
		{
            GameManager.instance.OnClickResetGame();
		}
		else if(currentGameStatus.Equals(GameStatus.win))
		{
			GameManager.instance.NextLevel();
		}
	}
}

