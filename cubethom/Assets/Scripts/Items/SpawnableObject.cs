﻿
using UnityEngine;

[System.Serializable]
public class SpawnableObject 
{
	[SerializeField] GameObject _objectPrefab;
	public GameObject ObjectPrefab { get { return _objectPrefab; } }

	[SerializeField] float _spawnRate;
	public float SpawnRate { get { return _spawnRate; } }
}
