﻿using UnityEngine;

public class Items : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		Debug.Log("triggered");
		ItemEffect();
	}

	public virtual void ItemEffect()
	{
		Destroy(this.gameObject);
	}
}
