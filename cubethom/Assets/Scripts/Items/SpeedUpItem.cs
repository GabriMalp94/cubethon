﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpItem : Items
{
	[SerializeField] float speedModifier = 1;

	public override void ItemEffect()
	{
		GameManager.instance.MapSpeed += speedModifier;
		Debug.Log("speed up");
		base.ItemEffect();
	}
}
