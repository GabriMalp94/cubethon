﻿using UnityEngine;

public class obstacle : MonoBehaviour
{
	private void OnCollisionEnter(Collision collision)
	{
		PlayerGameOver(collision.gameObject);
		
	}

	private void PlayerGameOver(GameObject collision)
	{
		if (collision.gameObject.TryGetComponent<PlayerController>(out PlayerController currentPlayer))
		{
			GameManager.instance.GameOver();
		}
	}
}
