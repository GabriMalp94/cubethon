﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[SerializeField] float speed = 10;

	private Rigidbody PlayerRb;


	private void Awake()
	{
		PlayerRb = GetComponent<Rigidbody>();
	}

	void Update()
    {
		if(GameManager.instance.CurrentGamestatus == GameStatus.runing)
		{
			Vector3 move = Vector3.zero;

			move.x = Input.GetAxis("Horizontal") * speed;

			PlayerRb.MovePosition(transform.position + move*Time.deltaTime);

		}

    }
}
