﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mapTile : MonoBehaviour
{
	[SerializeField] float breakSpeed = 2;
	[SerializeField] List<SpawnableObject> ObjectsTospawn;
	[SerializeField] private Items EndLevel;
	[SerializeField] Transform[] spawnPoints;

	[SerializeField] int minMapToSpawnObstacle = 5;
	private Vector3 currentMove;



	public void Setup()
	{
		if(GameManager.instance.CurrentMapCount>minMapToSpawnObstacle && GameManager.instance.CurrentGamestatus.Equals(GameStatus.runing) && !GameManager.instance.instantiatedEndLevel)
		{
			//Debug.Log(GameManager.instance.CurrentMapCount + " / " + GameManager.instance.MapTailToFinishLevel*GameManager.instance.DifficultModifier);
			if (GameManager.instance.CurrentMapCount >= GameManager.instance.MapTailToFinishLevel*GameManager.instance.DifficultModifier)
			{
				Instantiate(EndLevel, spawnPoints[2]);
				GameManager.instance.instantiatedEndLevel = true;
			}
			else
			{
				float randomPercent = Random.Range(0, 100);
				float currentPerc = 0;
				foreach (SpawnableObject ob in ObjectsTospawn)
				{
					if (currentPerc + ob.SpawnRate * GameManager.instance.DifficultModifier > randomPercent )
					{
						int randomIndex = Random.Range(0, spawnPoints.Length);
						Instantiate(ob.ObjectPrefab, spawnPoints[randomIndex]);
						break;
					}
					else
					{
						currentPerc += ob.SpawnRate;
					}
				}
			}
		}
	}

	void Update()
    {
		if (GameManager.instance.CurrentGamestatus == GameStatus.runing)
		{
			currentMove = Vector3.back * GameManager.instance.MapSpeed * GameManager.instance.DifficultModifier;
		}
		else
		{
			currentMove = Vector3.Lerp(currentMove, Vector3.zero, Time.deltaTime*breakSpeed);
			if(currentMove == Vector3.zero)
			{
				GameManager.instance.StopGame();
				Debug.Log("stop game");
			}
		}


		transform.position += currentMove * Time.deltaTime;
    }
}
