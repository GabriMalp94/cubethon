﻿using UnityEngine;

public class MapCleaner : MonoBehaviour
{

	private void OnTriggerEnter(Collider other)
	{
		GameManager.instance.newTile();
		Destroy(other.gameObject);
	}
}
